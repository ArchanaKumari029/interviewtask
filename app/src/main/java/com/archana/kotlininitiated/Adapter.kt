package com.archana.kotlininitiated

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bhuva.kotlininitiated.databinding.AdapterChildBinding

class Adapter(var arrayList: ArrayList<String>) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AdapterChildBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    fun updateList(updateList: ArrayList<String>) {
        arrayList = updateList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text = arrayList[position]
    }

    class ViewHolder(val binding: AdapterChildBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return arrayList.size
    }
}

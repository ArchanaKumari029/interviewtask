package com.archana.kotlininitiated

import android.app.Application
 import com.archana.kotlininitiated.di.AppComponent
import com.archana.kotlininitiated.di.AppModule
import com.archana.kotlininitiated.di.BaseViewModelModule
import com.bhuva.kotlininitiated.di.DaggerAppComponent

class InterViewApplication : Application() {
    private lateinit var appComponent: AppComponent

     override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .baseViewModelModule(
                BaseViewModelModule(
                    this
                )
            )
            .build()
         appComponent.inject(this)

    }

    fun getComponent(): AppComponent {
        return appComponent
    }
}
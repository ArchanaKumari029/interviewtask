package com.archana.kotlininitiated.movies

import android.app.Application
import com.archana.kotlininitiated.WebService
import com.archana.kotlininitiated.di.BaseRepository
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class MovieListRepository @Inject constructor(
    application: Application,
    override var service: WebService
) : BaseRepository(application, service) {


    fun dataFromApi(): ArrayList<String> {

        val call = service.getMovies()

        var list = ArrayList<String>()
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var jsonArray = JSONArray(response.body()?.string())

                for (i in 0 until jsonArray.length()) {
                    var title = jsonArray.getJSONObject(i).getString("title")
                    list.add(title)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                print("falureassd" + t.toString())
            }

        })
        return list

    }


}
package com.archana.kotlininitiated.movies

import android.app.Application
import androidx.lifecycle.*
import java.lang.IllegalArgumentException
import javax.inject.Inject

class MovieListViewModel(application: Application, private val movieListRepository: MovieListRepository) :
    AndroidViewModel(application) {


    fun getMovieListDetails(): LiveData<ArrayList<String>> {
        var list = MutableLiveData<ArrayList<String>>()
        list.value = movieListRepository.dataFromApi()
        return list
    }


    @Suppress("UNCHECKED_CAST")
    class Factory @Inject constructor(
        val application: Application,
        private val movieListRepository: MovieListRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {

            return MovieListViewModel(
                application,
                movieListRepository
            ) as T
            throw IllegalArgumentException("Unknown Exception")
        }
    }

}
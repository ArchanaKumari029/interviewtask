package com.archana.kotlininitiated

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface WebService {

    @GET("movies")
    fun getMovies(): Call<ResponseBody>
}
package com.archana.kotlininitiated

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.archana.kotlininitiated.movies.MovieListViewModel
import com.bhuva.kotlininitiated.R
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MovieListViewModel
    @Inject
    lateinit var factory: MovieListViewModel.Factory


    lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as InterViewApplication).getComponent().inject(this)
        viewModel = this.let { ViewModelProviders.of(it, factory).get(MovieListViewModel::class.java) }

        adapter = Adapter(arrayListOf())
        recyclerView.adapter = adapter

        viewModel.getMovieListDetails().observe(this, Observer {
            it.let {
                adapter.updateList(it)
            }
        })

    }

}

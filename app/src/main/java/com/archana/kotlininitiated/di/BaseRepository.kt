package com.archana.kotlininitiated.di

import android.app.Application
import com.archana.kotlininitiated.WebService

open class BaseRepository(
    protected val application: Application,
    protected open val service: WebService
)
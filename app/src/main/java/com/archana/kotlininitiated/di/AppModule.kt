package com.archana.kotlininitiated.di

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class AppModule(app: Application) {
    private val application: Application = app
    @Provides
    fun provideApplication(): Application {
        return application
    }

}
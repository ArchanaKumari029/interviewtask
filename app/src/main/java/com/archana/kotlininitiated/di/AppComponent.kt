package com.archana.kotlininitiated.di

import com.archana.kotlininitiated.MainActivity
import com.archana.kotlininitiated.InterViewApplication
import com.archana.kotlininitiated.WebService
import com.archana.kotlininitiated.movies.MovieListRepository
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, BaseViewModelModule::class])
interface AppComponent {


    fun inject(application: InterViewApplication)

    fun inject(mainActivity: MainActivity)

    fun inject(webService: WebService)

    fun inject(movieListRepository: MovieListRepository)

}